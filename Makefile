CC = gcc
CFLAGS = -Wall

LD = gcc
LFLAGS = -Wall

MM_OBJS = 	./src/main.o \
			./src/memory_manager.o \
			./src/requests_creator.o \
			./src/swap.o \
			./src/allocations.o \
			./src/structures.o

EXECS = mm

all: $(MM_OBJS)
	$(MAKE) -C ./src all
	$(LD) $(LFLAGS) -o $(EXECS) $(MM_OBJS)
	
clean:
	$(MAKE) -C ./src clean
	rm -f *~
	rm $(EXECS)

