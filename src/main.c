#include "main.h"

int main(int argc, char* argv[])
{
    
    printf("\n\nI woke up!\n\n");
    
    int threads_no = 20;
    int pages_no = 50;
    int wsl = 4; // work set limit.
    int memory_size = 64; // total de páginas que cabe na memória.

	if (argc == 3){
		threads_no = atoi(argv[1]);
		pages_no = atoi(argv[2]); 	
	}
    
    printf("\nInput: threads=%d; pages_by_threads=%d; wsl=%d; memory_size=%d\n", threads_no, pages_no, wsl, memory_size);
    
	Requests* buffer = allocateSharedRequests(memory_size);
    
    int pid = fork();
    
    if(pid == 0){

        printf("\nI am the process that create the requests!\n");
       
		srand (time(NULL));

		/* chamada à função que cria requisições passando as variáveis begin_list, end_list,
		 número de threads e o número de páginas por thread como parâmetro*/ 

		create_requests(threads_no, pages_no, buffer);

    }
    else{
        printf("\nI am the process that manage the memory!\n");
        // chama a função que gerencia a memória passando as variáveis begin_list e end_list como parâmetro.
		managesMemory(memory_size, wsl, buffer);
		waitpid(pid, NULL, 0);

		printf("Oba, filho terminou\n");
    }
    
    return 0;
}
