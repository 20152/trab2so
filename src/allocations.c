#include "allocations.h"

void freeSwapThread(SwapThread* swapthread){
	SwapPage* temp_page = swapthread->pages;
	SwapPage* aux_page = NULL;
	while(temp_page != NULL){
		aux_page = temp_page;
		temp_page = temp_page->next;
		free(aux_page);
	}
	free(swapthread);
}

SwapPage* allocateSwapPage(){
	SwapPage* page = (SwapPage*)malloc(sizeof(SwapPage));
	if(page == NULL){
		printf("\nError to allocate SwapPage\n");
		exit(-1);
	}
	page->id = EMPTY;
	page->next = NULL;
	
	return page;
}

SwapThread* allocateSwapThread(){
	SwapThread* thread = (SwapThread*)malloc(sizeof(SwapThread));
	if(thread == NULL){
		printf("\nError to allocate SwapPage\n");
		exit(-1);
	}
	thread->id = EMPTY;
	thread->pages = NULL;
	thread->next = NULL;
	
	return thread;
}


void freeThread(Memory* memory, Thread* thread){
	FramesList* temp_frame = thread->frames;
	FramesList* next_frame = NULL;
	
	while(temp_frame != NULL){
		freePageInMemory(memory, temp_frame->id);
		next_frame = temp_frame->next;
		free(temp_frame);
		temp_frame = next_frame;
	}
	
	free(thread);
}

void freePageInMemory(Memory* memory, int index){
	Page* old_page = memory->frames[index]->page;
	free(old_page);
	memory->frames[index]->page = NULL;
}

Memory* allocateMemory(int size){
	Memory* m = (Memory*)malloc(sizeof(Memory));
	if(m == NULL){
		printf("\nError to allocate memory!\n");
		exit(-1);
	}
	
	Frame** frames = (Frame**)malloc(sizeof(Frame*) * size);
	if(frames == NULL){
		printf("\nError to allocate frames list!\n");
		exit(-1);
	}
	
	int i;
	for(i=0; i<size; i++){
		frames[i] = allocateFrame();
	}
	
	m->threads = NULL;
	m->size = size;
	m->frames = frames;
	m->swapthreads = NULL;
	
	return m;
}

Thread* allocateThread(int wsl){
	Thread* p = (Thread*)malloc(sizeof(Thread));
	if(p == NULL){
		printf("\nError to allocate thread\n");
		exit(-1);
	}
 
	p->id = -1;
	p->frames = NULL;
	p->wsl = wsl;
	
	return p;
}

FramesList* allocateFramesList(){
	FramesList* frames_list = (FramesList*)malloc(sizeof(FramesList));
	if(frames_list == NULL){
		printf("\nError to allocate frames list!\n");
		exit(-1);
	}
	
	frames_list->id = EMPTY;
	frames_list->next = NULL;
	return frames_list;
}

Frame* allocateFrame(){
	Frame* f = (Frame*)malloc(sizeof(Frame));
	if(f == NULL){
		printf("\nError to allocate frame!\n");
		exit(-1);
	}
	
	f->page = NULL;
	
	return f;
}

Page* allocatePage(){
	Page* p = (Page*)malloc(sizeof(Page));
	if(p == NULL){
		printf("\nError to allocate page!\n");
		exit(-1);
	}
	
	p->thread = EMPTY;
	p->id = EMPTY;
	
	return p;
}
