#include "memory_manager.h"

void managesMemory(int memory_size, int wsl, Requests* buffer){
	Memory* memory = allocateMemory(memory_size);
	
	Page* page  = NULL;
	Thread* thread = NULL;
	SwapThread* swapthread = NULL;
	
	while(1){
		page = getPageFromRequest(buffer);
		thread = getThreadById(memory, page->thread);
		
		if(thread == NULL){
			swapthread = getSwapedOutThreadById(memory, page->thread);
			
			if(swapthread == NULL){
				thread = createNewThreadInMemory(memory, wsl, page->thread);
			}
			else{
				thread = swapInThread(memory, swapthread, wsl);
			}
		}
		
		insertPageByLRU(memory, thread, page);
		printSwapThreads(memory);
		printThreads(memory);
		printMemory(memory);
	}
}

void printThreads(Memory* memory){
	Thread* thread = memory->threads;
	
	printf("\nThreads running:");
	while(thread != NULL){
		printThread(memory, thread);
		thread = thread->next;
	}
	
	printf("\n\n");
}

void printThread(Memory* memory, Thread* thread){
	printf("\nThread %d: {",thread->id);
	Page* page = NULL;
	FramesList* frame = thread->frames;
	if(frame == NULL){
		printf("NULL}");
		return;
	}
	while(frame != NULL){
		page = getPageFromMemory(memory, frame->id);
		if(frame->next == NULL){
			printf("(frame: %d, page: %d)}", frame->id, page->id);
		}
		else{
			printf("(frame: %d, page: %d), ", frame->id, page->id);
		}
		frame = frame->next;
	}
	
}

Page* getPageFromMemory(Memory* memory, int index){
	if(index < 0 || index >= memory->size){
		return NULL;
	}
	else{
		return memory->frames[index]->page;
	}
}

void printMemory(Memory* memory){
	int i;
	Page* page;
	printf("\nMemory:");
	for(i=0; i<memory->size; i++){
		page = memory->frames[i]->page;
		
		if(i%4 == 0){
			printf("\n");
		}
		
		if(page != NULL){
			printf("pos: %d {thread: %d, page: %d}", i, page->thread, page->id);
		}
		else{
			printf("pos: %d {NULL}", i);
		}
		
		if(i != (memory->size-1)){
			printf(",\t");
		}
	}
	
	printf("\n\n");
}

Page* getPageFromRequest(Requests* buffer){
	Page* page = allocatePage();
	Request* request = NULL;
	
	request = removeRequestFromBuffer(buffer);
	
	page->id = request->page;
	page->thread = request->thread;
	
	free(request);
	
	printf("\nReading the request: thread=%d; page=%d\n", page->thread, page->id);
	
	return page;
}

Thread* getThreadById(Memory* memory, int id){
	Thread* temp_thread = memory->threads;
	
	while(temp_thread != NULL){
		if(temp_thread->id == id){
			break;
		}
		temp_thread = temp_thread->next;
	}
	
	if(temp_thread != NULL)
		printf("\nThread %d found in memory!\n", temp_thread->id);
	
	return temp_thread;
}

Thread* createNewThreadInMemory(Memory* memory, int wsl, int id){
	Thread* thread = allocateThread(wsl);
	thread->id = id;
	thread->next = memory->threads;
	memory->threads = thread;
	
	return thread;
}

void insertPageByLRU(Memory* memory, Thread* thread, Page* page){
	if(thread->id != page->thread){
		printf("\nError: the LRU function can't insert a page from diferent thread.\n");
		exit(-1);
	}
	
	putThreadOnTop(memory, thread);
	
	if(isPageInThread(memory, thread, page)){
		updatePageInThread(memory, thread, page);
	}
	else{
		if(isThereSpaceInThread(thread)){
			insertNewPageInThread(memory, thread, page);
		}
		else{
			replacesPageInThread(memory, thread, page);
		}
	}
	
}

void putThreadOnTop(Memory* memory, Thread* thread){

	if(thread == memory->threads){
		return;
	}

	Thread* before_proc = memory->threads;
	Thread* after_proc = memory->threads;
	
	while(after_proc != NULL){
		
		if(after_proc->id == thread->id){
			break;
		}
		
		before_proc = after_proc;
		after_proc = after_proc->next;
	}
	
	before_proc->next = after_proc->next;
	after_proc->next = memory->threads;
	memory->threads = after_proc;
}

int isPageInThread(Memory* memory, Thread* thread, Page* page){
	FramesList* temp_frame = thread->frames;
	Page* temp_page;
	
	while(temp_frame != NULL){
		temp_page = memory->frames[temp_frame->id]->page;
		
		if(temp_page->id == page->id){
			printf("\nPage was found in memory!\n");
			return TRUE;
		}
		
		temp_frame = temp_frame->next;
	}
	
	return FALSE;
}

int isThereSpaceInThread(Thread* thread){
	FramesList* temp_frame = thread->frames;
	int count_frames = 0;
	
	while(temp_frame != NULL){
		count_frames++;
		temp_frame = temp_frame->next;
	}
	
	if(count_frames < thread->wsl){
		return TRUE;
	}
	else{
		return FALSE;
	}
}

void replacesPageInThread(Memory* memory, Thread* thread, Page* page){
	FramesList* after_frame = thread->frames;
	FramesList* before_frame = thread->frames;
	
	while(after_frame->next != NULL){
		before_frame = after_frame;
		after_frame = after_frame->next;
	}
	
	
	if(after_frame != thread->frames){
		before_frame->next = NULL;
		after_frame->next = thread->frames;
		thread->frames = after_frame;
	}
	
	replacesPageInMemory(memory, page, after_frame->id);
	
	printf("\nThe page %d from thread %d will replace another page in memory %d.\n", page->id, page->thread, after_frame->id);
}

void updatePageInThread(Memory* memory, Thread* thread, Page* page){
	if(thread->frames == NULL){
		printf("\nError: frames list is null. The function can't replace page in thread.\n");
		exit(-1);
	}
	
	FramesList* before_frame = thread->frames;
	FramesList* after_frame = thread->frames;
	Page* temp_page;
	
	while(after_frame != NULL){
		temp_page = memory->frames[after_frame->id]->page;
	
		if(temp_page->id == page->id){
			break;
		}
		
		before_frame = after_frame;
		after_frame = after_frame->next;
	}
	
	if(after_frame != thread->frames){
		before_frame->next = after_frame->next;
		after_frame->next = thread->frames;
		thread->frames = after_frame;
	}
	
	printf("\nThe position of page %d from thread %d in memory %d was update\n", page->id, page->thread, thread->frames->id);
	
	free(page);
	
}

void insertNewPageInThread(Memory* memory, Thread* thread, Page* page){
	int index = insertNewPageInMemory(memory, page);
	if(index == MEMORY_FULL){
		swapOutOldestThread(memory);
		index = insertNewPageInMemory(memory, page);
	}
	
	FramesList* frame = allocateFramesList();
	frame->id = index;
	
	frame->next = thread->frames;
	thread->frames = frame;
	
	printf("\nThe new page %d from thread %d was inserted in memory %d.\n", page->id, page->thread, index);
}

void replacesPageInMemory(Memory* memory, Page* page, int index){
	freePageInMemory(memory, index);
	memory->frames[index]->page = page;
}




