#include "requests_creator.h"

int generate_random_number(int max){

	return rand() % max;
}

int generate_random_thread(int threads[], int* remaining_threads){

	int random_index = generate_random_number(*remaining_threads);
	int thread_chosen = threads[random_index];

	*remaining_threads -= 1;

	threads[random_index] = threads[*remaining_threads];

	return thread_chosen;
	
}

void fill_array_of_threads(int* threads, int threads_created){

	int i;

	for (i = 0; i < threads_created; i++)
		threads[i] = i;

}

void create_requests(int threads_no, int pages_no, Requests* buffer){

	int threads_created = 1;
	int *remaining_threads = (int*) malloc(sizeof(int));

	int *threads = (int*) malloc(sizeof(int)*threads_no);

	while (1){

		*remaining_threads = threads_created;

		printf("\nNumero de threads = %d\n", threads_created);

		fill_array_of_threads(threads, threads_created);

		while (*remaining_threads > 0){

			int thread = generate_random_thread(threads, remaining_threads);
			int page = generate_random_number(pages_no);

			printf("Requisicao: Thread = %d Página = %d\n", thread, page);

			insertRequestInBuffer(buffer, thread, page);
		}

		sleep(2);

		if (threads_created < threads_no)
			threads_created++;
		
	}

	free(threads);
	free(remaining_threads);

}
