#include "structures.h"

Request* removeRequestFromBuffer(Requests* buffer){
	Request* request = NULL;
	
	int out = buffer->out;
	while(isEmptyRequest(buffer->requests[out])){
		sleep(1);
	}
	
	if(!isEmptyRequest(buffer->requests[out])){
		request = cloneRequest(buffer->requests[out]);
		clearRequest(buffer->requests[out]);
		buffer->out = (buffer->out+1) % buffer->size;
	}
	else{
		printf("\nError the buffer is empty.\n");
	}
	
	return request;
}

void insertRequestInBuffer(Requests* buffer, int thread, int page){
	
	int in = buffer->in;
	while(isFullRequest(buffer->requests[in])){
		sleep(1);
	}
	
	if(!isFullRequest(buffer->requests[in])){
		buffer->requests[in]->thread = thread;
		buffer->requests[in]->page = page;
		buffer->in = (buffer->in+1) % buffer->size;
	}
	else{
		printf("\nError the buffer is full.\n");
	}
}

int isEmptyRequest(Request* r){
	if(r->thread == -1 || r->page == -1){
		return TRUE;
	}
	else{
		return FALSE;
	}
}

int isFullRequest(Request* r){
	if(r->thread != -1 || r->page != -1){
		return TRUE;
	}
	else{
		return FALSE;
	}
}


void clearRequest(Request* r){
	r->thread = -1;
	r->page = -1;
}

Request* cloneRequest(Request* r){
	Request* clone = allocateRequest();
	clone->thread = r->thread;
	clone->page = r->page;
	return clone;
}

Requests* allocateSharedRequests(int size){
	Requests* buffer = (Requests*)mmap(0, sizeof(Requests), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (buffer == MAP_FAILED) {
        printf("\nError to map requests2!\n");
        exit(-1);
    }
    
    buffer->requests = (Request**)mmap(0, sizeof(Request*) * size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (buffer->requests == MAP_FAILED) {
        printf("\nError to map requests2 list in requests!\n");
        exit(-1);
    }
    
    buffer->size = size;
    int i;
    for(i=0; i<size; i++){
    	buffer->requests[i] = allocateSharedRequest();
    }
    
    buffer->in = 0;
    buffer->out = 0;
    
    return buffer;
}

Request* allocateSharedRequest(){
    Request* request = (Request*)mmap(0, sizeof(Request), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (request == MAP_FAILED) {
        printf("\nError to map request2!\n");
        exit(-1);
    }

    request->thread = -1;
    request->page = -1;
    return request;
}

Request* allocateRequest(){
    Request* request = (Request*)malloc(sizeof(Request));
    if (request == NULL) {
        printf("\nError to allocate request2!\n");
        exit(-1);
    }

    request->thread = -1;
    request->page = -1;
    return request;
}
