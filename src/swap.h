#include "allocations.h"

int insertNewPageInMemory(Memory* memory, Page* page);
void swapOutOldestThread(Memory* memory);
void swapOutThread(Memory* memory, Thread* thread);

SwapThread* getSwapedOutThreadById(Memory* memory, int id);
Thread* swapInThread(Memory* memory, SwapThread* swapthread, int wsl);

void printSwapThreads(Memory* memory);
void printSwapThread(SwapThread* swapthread);
