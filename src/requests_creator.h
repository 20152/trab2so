#ifndef REQUESTS_CREATOR
#define REQUESTS_CREATOR

#include <limits.h>

#include "structures.h"

void create_requests(int threads_no, int pages_no, Requests* buffer);

int generate_random_number(int max);

int generate_random_thread(int threads[], int* remaining_threads);

void fill_array_of_threads(int* threads, int threads_created);

#endif
