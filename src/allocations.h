#include "structures.h"

Page* allocatePage();
Frame* allocateFrame();
FramesList* allocateFramesList();
Thread* allocateThread(int wsl);
Memory* allocateMemory(int size);

void freePageInMemory(Memory* memory, int index);
void freeThread(Memory* memory, Thread* thread);

SwapThread* allocateSwapThread();
SwapPage* allocateSwapPage();

void freeSwapThread(SwapThread* swapthread);

