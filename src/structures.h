#ifndef STRUCTURES
#define STRUCTURES


#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/mman.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>


#define SUCCESS -7
#define ERROR -8
#define MEMORY_FULL -9
#define EMPTY -1

#ifndef TRUE
#define TRUE 1;
#endif

#ifndef FALSE
#define FALSE 0;
#endif

//Structs to Swap.
typedef struct _swappage{
	int id;
	struct _swappage* next;
} SwapPage;

typedef struct _swapthread{
	int id;
	SwapPage* pages;
	struct _swapthread* next;
} SwapThread;

//Structs to Memory Manager
typedef struct _page{
	int thread;
	int id;
} Page;

typedef struct _frame{
	Page* page;
} Frame;

typedef struct _frameslist{
	int id;
	struct _frameslist* next;
} FramesList;

typedef struct _thread{
	int id;
	int wsl;
	FramesList* frames;
	struct _thread* next;
} Thread;

typedef struct _memory{
	int size;
	Thread* threads;
	Frame** frames;
	SwapThread* swapthreads;
} Memory;


//Structs to shared memory
typedef struct _request{
	int thread;
	int page;
} Request;

typedef struct _requests{
	int size;
	int in;
	int out;
	Request** requests;
} Requests;

Request* allocateRequest();
Request* allocateSharedRequest();
Requests* allocateSharedRequests(int size);
Request* cloneRequest(Request* r);
void clearRequest(Request* r);
int isEmptyRequest(Request* r);
int isFullRequest(Request* r);
void insertRequestInBuffer(Requests* buffer, int thread, int page);
Request* removeRequestFromBuffer(Requests* buffer);

#endif
