#ifndef MEMORY_MANAGER
#define MEMORY_MANAGER

#include "swap.h"

void replacesPageInMemory(Memory* memory, Page* page, int index);
void insertNewPageInThread(Memory* memory, Thread* thread, Page* page);
void updatePageInThread(Memory* memory, Thread* thread, Page* page);
void replacesPageInThread(Memory* memory, Thread* thread, Page* page);

int isPageInThread(Memory* memory, Thread* thread, Page* page);
int isThereSpaceInThread(Thread* thread);
void putThreadOnTop(Memory* memory, Thread* thread);
void insertPageByLRU(Memory* memory, Thread* thread, Page* page);

Thread* createNewThreadInMemory(Memory* memory, int wsl, int id);
Thread* getThreadById(Memory* memory, int id);
Page* getPageFromRequest(Requests* buffer);

void printMemory(Memory* memory);
Page* getPageFromMemory(Memory* memory, int index);
void printThread(Memory* memory, Thread* thread);
void printThreads(Memory* memory);

void managesMemory(int memory_size, int wsl, Requests* requests);

#endif
