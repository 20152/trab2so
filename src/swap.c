#include "swap.h"

void printSwapThread(SwapThread* swapthread){
	SwapPage* swappage = swapthread->pages;
	printf("\nThread %d: {", swapthread->id);
	if(swappage==NULL){
		printf("NULL}");
	}
	
	while(swappage != NULL){
		if(swappage->next == NULL){
			printf("page: %d}", swappage->id);
		}
		else{
			printf("page: %d, ", swappage->id);
		}
		swappage = swappage->next;
	}
}

void printSwapThreads(Memory* memory){
	SwapThread* swapthread = memory->swapthreads;
	printf("\nSwaped Out Threads:");
	while(swapthread != NULL){
		printSwapThread(swapthread);
		swapthread = swapthread->next;
	}
	
	printf("\n\n");
}

Thread* swapInThread(Memory* memory, SwapThread* swapthread, int wsl){

	if(swapthread->pages == NULL){
		printf("\nError: the swapthread must have at least one swappage.\n");
		exit(-1);
	}
	
	SwapPage* temp_swappage = swapthread->pages;
	Page* page;

	FramesList* framelist = allocateFramesList();
	Thread* thread = allocateThread(wsl);

	thread->id = swapthread->id;
	thread->frames = framelist;

	while(temp_swappage != NULL){

		page = allocatePage();
		page->thread = swapthread->id;
		page->id = temp_swappage->id;

		framelist->id = insertNewPageInMemory(memory, page);
		if(framelist->id == MEMORY_FULL){
			swapOutOldestThread(memory);
			framelist->id = insertNewPageInMemory(memory, page);
		}
		
		if(temp_swappage->next != NULL){
			framelist->next = allocateFramesList();
			framelist = framelist->next;
		}
		
		temp_swappage = temp_swappage->next;
	}
	
	thread->next = memory->threads;
	memory->threads = thread;
	
	freeSwapThread(swapthread);
	
	printf("\nThe thread %d was swaped in.\n", thread->id);

	return thread;
}

SwapThread* getSwapedOutThreadById(Memory* memory, int id){

	SwapThread* before_swapthread = memory->swapthreads;
	SwapThread* after_swapthread = memory->swapthreads;
	
	while(after_swapthread != NULL){

		if (after_swapthread->id == id){
			if(before_swapthread == after_swapthread){
				memory->swapthreads = after_swapthread->next;
			}
			else{
				before_swapthread->next = after_swapthread->next;
			}
			after_swapthread->next = NULL;
			break;
		}
		before_swapthread = after_swapthread;
		after_swapthread = after_swapthread->next;
			
	}

	return after_swapthread;
}

void swapOutThread(Memory* memory, Thread* thread){

	if(thread->frames == NULL){
		printf("\nError the program can't swap out a thread that there isn't in memory.\n");
		exit(-1);
	}
	
	SwapPage* swappage = allocateSwapPage();
	Page* page;
	
	SwapThread* swapthread = allocateSwapThread();
	swapthread->id = thread->id;
	swapthread->pages = swappage;
	
	
	FramesList* framelist = thread->frames;
	
	while(framelist != NULL){
		
		page = memory->frames[framelist->id]->page;
		swappage->id = page->id;
		
		if(framelist->next != NULL){
			swappage->next = allocateSwapPage();
			swappage = swappage->next;
		}
		
		framelist = framelist->next;
	}
	
	swapthread->next = memory->swapthreads;
	memory->swapthreads = swapthread;
	
	freeThread(memory, thread);

}

void swapOutOldestThread(Memory* memory){
	Thread* before_thread = memory->threads;
	Thread* after_thread = memory->threads;
	
	while(after_thread->next != NULL){		
		before_thread = after_thread;
		after_thread = after_thread->next;
	}
	
	before_thread->next = NULL;
	
	if(after_thread == memory->threads){
		memory->threads = NULL;
	}
	
	int id = after_thread->id;
	swapOutThread(memory, after_thread);
	
	printf("\nThe thread %d was swaped out!\n", id);
}

int insertNewPageInMemory(Memory* memory, Page* page){
	int i;
	for(i=0; i<memory->size; i++){
		if(memory->frames[i]->page == NULL){
			memory->frames[i]->page = page;
			return i;
		}
	}
	
	return MEMORY_FULL;
}


